@extends('layouts.app')
@section('style')
h1 {
    display: inline-block;
    background-color: #333;
    color: #fff;
    font-size: 20px;
    font-weight: normal;
    text-transform: uppercase;
    padding: 20px;
    float: left;
}
.clear {
    clear: both;
}
.items {
    display: block;
    margin: 0px;
}
.item {
    background-color: #fff;
    float: left;
    margin: 0 10px 10px 0;
    width: 205px;
    padding: 5px;
    height: 220px;
    border:1px solid #ddd;
    border-radius:5px;


}

h2 {
    font-size: 16px;
    display: block;
    overflow: hidden;
    border-bottom: 1px solid #ccc;
    margin: 0 0 10px 0;
    padding: 0 0 5px 0;
    }
.add-to-cart {

    color:white;

}
.shopping-cart {
    display: inline-block;
    background: url('http://cdn1.iconfinder.com/data/icons/jigsoar-icons/24/_cart.png') no-repeat 0 0;
    width: 24px;
    height: 24px;
    margin: 0 10px 0 0;
}
#sidebar { 
  	position: fixed; 
  	width: 80px;
  	top:55px;
  	right: 0px; 
  	vertical-align: middle;
  	z-index:80;
}
a
{
	text-decoration: none;
	border:0px;
}
.top0{
	top:10px !important;
}
.top55
{
top:55px !important;
}
.padding
{
padding:20px;
}
@stop


@section('content')
{{-- <button data-bind="click:check">asdasd</button> --}}
<!-- wrapper -->
<div class="padding row" id="top">
    <h1>MOO Stock</h1>
    <div id="sidebar" class="text-center">
			<span data-toggle="modal" data-target="#itemList"><i class="shopping-cart"></i></span><br>
			<span data-bind="text:Count"></span> piece(s)
	</div>
</div>

    <div class="clear"></div>
    <!-- items -->
<div data-bind="foreach:ProductItems" class="row text-center">

	<div class="col-md-3 col-sm-6">
	    <div class="thumbnail">
	    	<div>
	        <img data-bind="attr:{src: Image}" alt="No Image" class="img-responsive">
	        </div
	        <div class="caption">
	        <div>
	          <h2 data-bind="text:Name"></h2>
	        </div>
	        <div style="height:60px;">
	             <p data-bind="visible:PromotionPrice != 0">
		             	<s><span data-bind="text:Price"></span> B</s>
		             	<p><span data-bind="text:PromotionPrice"></span> B</p>
		             </p>
		    </div>
	            <div>
	                <button class="add-to-cart btn btn-primary" data-bind="click: $parent.AddToList,FlyToCart:$parent.visible">Buy Now!</button> 
		            <button class="btn btn-default">More Info!</button>
	            </div>
        </div>
    </div>
</div>

    {{-- <div class="items text-center">

        <!-- single item -->
        <div class="item">
        	<div style="height:150px;">
            	<img data-bind="attr:{src: Image}" alt="No Image" width="190" height="150" style="max-height:150px;" />
            </div>
            <div>
            	<h2 data-bind="text:Name"></h2>
            </div>
             <div class="text-center" style="height:50px;width:100%;">
             	<div data-bind="visible:PromotionPrice">
	             <p data-bind="visible:PromotionPrice != 0">
	             	<s><span data-bind="text:Price"></span> B</s>
	             	<p><span data-bind="text:PromotionPrice"></span> B</p>
	             </p>
	             </div>
	             <p data-bind="if:PromotionPrice == 0"><span data-bind="text:Price"></span> B</p>
             </div>
             <div>
	            <button class="add-to-cart btn btn-primary" data-bind="click: $parent.AddToList,FlyToCart:$parent.visible">Buy Now!</button> 
	            <button class="btn btn-default">More Info!</button>
            </div>
        </div>
        <!--/ single item -->
    </div> --}}
    <!--/ items -->
<div class="row text-center">
		<div class="col-md-12" >
			<ul class="pagination" id="pagination-demo">
				
			</ul>
		</div>
</div>

<div class="modal fade" id="itemList" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Item List</h4>
      </div>
      <div class="modal-body">
	      <!--  <div data-bind="foreach:ProductItems">
			<div class="row">
				<div class="col-md-3"><span data-bind="text:Code"></span></div>
				<div class="col-md-3"><span data-bind="text:Name"></span></div>
				<div class="col-md-3">
				<button data-bind="click: $root.AddToList" class="btn btn-primary">+</button>
				<button data-bind="click: $root.RemoveFromList" class="btn btn-primary">-</button>
				</div>
			</div>
			</div> -->
			<div data-bind="if:!Count()" class="text-center">
				No item(s).
			</div>
			<div data-bind="if:Count()">
				<div class="row">
					<div class="col-md-2 text-center">Code</div>
					<div class="col-md-2 text-center">Name</div>
					<div class="col-md-2 text-center">piece(s)</div>
					<div class="col-md-2 text-center">Amount</div>
					<div class="col-md-2"></div>
				</div>	
				<div data-bind="foreach:Items">
				<div class="row" >
					<div class="col-md-2"><span data-bind="text:product.Code"></span></div>
					<div class="col-md-2"><span data-bind="text:product.Name"></span></div>
					<div class="col-md-2 text-right"><span data-bind="text:Count"></span></div>
					<div class="col-md-2 text-right"><span data-bind="text:Amount"></span></div>
					<div class="col-md-2">
						<button data-bind="click: $root.RemoveFromList" class="btn btn-primary">-</button>
						<button data-bind="click: $parent.AddToList" class="btn btn-primary">+</button>
					</div>
				</div>
				</div>
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-2"></div>
					<div class="col-md-2"></div>
					<div class="col-md-2 text-right">Total : <span data-bind="text: Total"></span></div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-bind="click:Payment,enable:Count()">Pay</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>

var Product = function()
{
	var self = this;
	self.Id = ko.observable(),
	self.Code = ko.observable(),
	self.Name = ko.observable(),
	self.Count = ko.observable(0)
};
var vm 	= new function()
	{
		

		var self = this;
		var gndPrice = 0;
		var data = {};
		
		self.visible = ko.observable(true);
		self.Count = ko.observable(0);
		self.Items = ko.observableArray();
		self.ProductList = ko.observableArray();
		self.ProductItems = ko.observableArray();
		self.productName = ko.observable();
		self.ModalVal = ko.observable(false);
		self.Pagination = ko.observableArray();
		self.limit = ko.observable(5);
		self.Total = ko.computed(function()
			{
				var item = self.Items();
				var total = 0;
				for(var i=0;i<self.Items().length;i++)
				{
					total+=item[i].Amount;
				}
				return total;
			});

		self.Ajax = function()
		{
			var sefl = this;
	        $.ajax({
	            type: 'GET',
	            crossDomain :true,
	            contentType: 'application/json; charset=utf-8',
	            url:'http://localhost:8000/GetProductWithPagination?limit='+self.limit(),
	            dataType: 'json',
	            async:false
	        }).done(function (result) {
	        	for(var i = 1;i <= result.last_page;i++)
	        			self.Pagination.push("http://localhost:8000/GetProductWithPagination?page="+i);

	            self.ProductItems(result.data);
	        }).fail(function (error) {
	           alert(error);
	        });
		};
		self.Ajax();
		function compare(a,b) {
		  if (a.Id < b.Id)
		    return -1;
		  if (a.Id > b.Id)
		    return 1;
		  return 0;
		}

		self.AddToList = function(e)
		{
			self.visible(!self.visible());
			if(typeof(e.Id) == 'undefined')
				self.ProductList.push(e.product);
			else
				self.ProductList.push(e);
			self.compressArray(self.ProductList());

		};
		self.RemoveFromList = function(e)
		{
			var items = self.ProductList();
			console.log("before::"+items);
			for(var i = 0; i <= items.length - 1; i++) {
			    if(items[i].Id == e.Id) {
			       	items.splice(i, 1);
			       	break;
			    }
			    if(items[i].Id == e.product.Id) {
			       	items.splice(i, 1);
			       	break;
			    }
			}
			console.log("###################################");
			console.log("Remove : "+items);
			console.log(items);
			console.log("###################################");
			self.ProductList(items);
			self.compressArray(self.ProductList());
		};
	
		self.Payment = function()
		{

			//data = self.convArrToObj(self.Items());
	        $.ajax({
	            type: 'POST',
	            crossDomain :true,
	            async:false,
	            contentType: 'application/json; charset=utf-8',
	            url:'http://localhost:8000/payment',
	            dataType: 'json',
	            data:JSON.stringify(self.Items())
	            //data:self.Items()
	        }).done(function (result) {
	        	localStorage.clear();
	        	self.Items.removeAll();
	        	self.Count(0);
	            alert('Waiting for transfer money...');

	        }).fail(function (error) {
	           alert(error);
	        });
	  		//localStorage.clear();
			// self.ProductList.removeAll();
			// self.Items.removeAll();
			// self.Count(0);
		};
		self.compressArray = function(original)
		{
			self.Items.removeAll();
		    var array_elements = original.slice(0);
		    self.Count(array_elements.length);
		    array_elements.sort(compare);
		    var current = null;
		    var cnt = 0;
		    for (var i = 0; i < array_elements.length; i++) {
		        if (array_elements[i].Id != current) {
		            if (cnt > 0) {
		            	var a = new Object();
						a.product = array_elements[i-1]
						a.Count= cnt;
						if(array_elements[i-1].PromotionPrice == 0)
							a.Amount = array_elements[i-1].Price*cnt;
						else
							a.Amount = array_elements[i-1].PromotionPrice*cnt;
						self.Items.push(a);
		                // document.write(current + ' comes --> ' + cnt + ' times<br>');
		            }
		            current = array_elements[i].Id;
		            cnt = 1;
		        } else {
		            cnt++;
		        }
		    }
		    if (cnt > 0) {
		    	var a = new Object();
				a.product = array_elements[i-1]
				a.Count= cnt;
				if(array_elements[i-1].PromotionPrice == 0)
					a.Amount = array_elements[i-1].Price*cnt;
				else
					a.Amount = array_elements[i-1].PromotionPrice*cnt;
				self.Items.push(a);
		        // document.write(current + ' comes --> ' + cnt + ' times');
		    }
		    localStorage.product = JSON.stringify(self.ProductList());
		}

		if(localStorage.product != null)
		{
			self.ProductList(JSON.parse(localStorage.product));
			self.compressArray(self.ProductList());
		}
	}	

	ko.bindingHandlers.slideIn = {
    init: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).toggle(value);
    },
    update: function (element, valueAccessor) {
    	alert(element);
        var value = ko.utils.unwrapObservable(valueAccessor());
        value ? $(element).slideDown() : $(element).slideUp();
    }
};

ko.bindingHandlers.FlyToCart = {
    init: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
    },
    update: function (element, valueAccessor) {
        $(element).on('click', function () {
        var cart = $('.shopping-cart');
        var imgtodrag = $(element).parent().parent().parent().find("img").eq(0);
        imgtodrag.attr('click');
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                $(imgclone).detach()
            });
        }
    });

    }
};

var wrap = $(window);
wrap.on("scroll", function(e) {
  if ($(window).scrollTop() > 0) {
    $("#sidebar").addClass("top0");
  } else {
    $("#sidebar").removeClass("top0");
  }

});
$('#pagination-demo').twbsPagination({
    totalPages: vm.Pagination().length,
    visiblePages: 7,
    href: '',
    onPageClick: function (event, page) {
    	$.getJSON( "http://localhost:8000/GetProductWithPagination?limit="+vm.limit()+"&page="+page)
			  .done(function( result ) {
	            vm.ProductItems(result.data);
	         
			  })
			  .fail(function( jqxhr, textStatus, error ) {
			   alert("fail");
			});
    }
});
ko.applyBindings(vm);
</script>
@stop