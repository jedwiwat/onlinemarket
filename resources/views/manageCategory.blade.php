@extends('layouts.app')
@section('style')
h1 {
    display: inline-block;
    background-color: #333;
    color: #fff;
    font-size: 20px;
    font-weight: normal;
    text-transform: uppercase;
    padding: 20px;
}
@stop
@section('content')



@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="wrapper row">
    <div class="col-md-10 col-md-offset-1">
    <h1>Add Category</h1>
        {!! Form::open(array('action' => 'CategoryController@Add', 'method' => 'post','style'=>'border:1px solid;border-radius:5px;border-color:gray;padding:10px;')) !!} 
           
        <div class="form-group">
        {!! Form::label('Name', 'Category Name:') !!}
        {!! Form::text('Name', null, ['class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            <input type="checkbox" name="IsActive" checked data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger">
        </div>
        {{Form::submit('Save',array('class' => 'btn btn-default'))}}
        {{Form::reset('Cancel',array('class' => 'btn btn-default'))}}
        {!! Form::close() !!}
    </div>    
</div>
<div class="wrapper row">
    <div class="col-md-10 col-md-offset-1">
    <table id="example" class="display" cellspacing="0" width="100%" data-bind="dataTable:triggerDataTable">
        <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    </div>
</div>

<br><br>
{{-- <div data-bind="foreach : CategoryItems">
    <p data-bind="text:Name"></p>
    <p data-bind="text:IsActive"></p>
</div> --}}
<script type="text/javascript">
var Category = function(data)
{
    var self= this;
    self.Id = ko.observable(data.Id),
    self.Name = ko.observable(data.Name)
    self.IsActive = ko.observable(data.IsActive)
};
var vm = new function() {
        var self = this;
        self.triggerDataTable = ko.observable(true);
        self.CategoryItems = ko.observableArray();
        self.table = "";
        self.GetCategory = function()
        {
            $.ajax({
                type: 'GET',
                crossDomain :true,
                contentType: 'application/json; charset=utf-8',
                url:'http://localhost:8000/GetCategory',
                dataType: 'json',
                async:false
            }).done(function (result) {
                var list = ko.observableArray();
                for(var i =0;i<result.length;i++)
                {
                    var a = new Category(result[i]);
                    list.push(a);
                }
                
                self.CategoryItems(list());
            }).fail(function (error) {
               alert(error);
            });
        };
        self.CallDataTable = function()
        {
            self.triggerDataTable(!self.triggerDataTable());
        }
        self.getIsActive = function()
        {

        }
    };

    ko.bindingHandlers.dataTable = {

    init: function(element, valueAccessor) {
        vm.GetCategory();
        $('#example').DataTable({
            data : vm.CategoryItems(),
            scrollX:true,
            // ajax: 
            // {
            //    url: 'http://localhost:8000/GetCategory',
            //    type: 'get',
            //    dataSrc: function ( json ) {
            //      debugger;
            //       return json;
            //     }
            // },
            columns: [
            // { data: 'Id' },
                { data: 'Name()' },
                { data: 'IsActive()' },
                {
                    sortable: false,
                    render: function ( data, type, full, meta ) {
                     var IsActive = full.IsActive;
                     var btn = "";
                    if(IsActive())
                    {
                        btn = "<input type=\"button\" style=\"width: 75px;\" class=\"btn btn-success\" data-toggle=\"button\" aria-pressed=\"false\" autocomplete=\"off\" value=\"Active\">";
                    }
                    else
                    {
                         btn = "<input type=\"button\" style=\"width: 75px;\" class=\"btn btn-danger\" data-toggle=\"button\" aria-pressed=\"false\" autocomplete=\"off\" value=\"Inactive\">";
                    }
                    return btn+" <button class='btn btn-danger' style=\"width: 75px;\">Delete</button>";
                     // return "<input type=\"checkbox\" data-bind=\"checked: "+IsActive()+" ==1 ? 'on':null\" name=\"IsActive\" checked data-toggle=\"toggle\" data-on=\"Active\" data-off=\"Inactive\" data-onstyle=\"success\" data-offstyle=\"danger\"/><button class='btn btn-danger'>Delete</button>";
                 }
                }
            ],
        });
    },
    update: function(element, valueAccessor){
        //ko.unwrap(valueAccessor());
        $('#example tbody').on( 'click', ' input[type="button"]', function () {
        var data = $('#example').DataTable().row($(this).parents('tr')).data();
        //var index = $('#example').DataTable().column(0).data().indexOf( data.Name() );
        //index = (vm.CategoryItems().length- index)-1;
        //var cell = $('#example').DataTable().cell(index,1);
        var el = this;
        var index = vm.CategoryItems().indexOf(data);
        
        $.ajax({
                type: 'POST',
                crossDomain :true,
                contentType: 'application/json; charset=utf-8',
                url:'http://localhost:8000/ToggleCategory',
                dataType: 'json',
                data: JSON.stringify({Id:data.Id()}),
                async:false
            }).done(function (result) {
                vm.CategoryItems()[index].IsActive(data.IsActive() == true? 0:1);
                $('#example').DataTable().row(index).invalidate().draw();
                // var a = $("#example").dataTable()
                // a.fnDestroy();
               
                //$('#example').DataTable().row($(el).parents('tr')).draw();
            }).fail(function (error) {
            });


        console.log(data);
        //alert( data[0] +"'s salary is: "+ data[ 1 ] );
    });

    $('#example tbody').on( 'click', 'button', function () {
        var data = $('#example').DataTable().row($(this).parents('tr')).data();
        var el = this;
        var index = vm.CategoryItems().indexOf(data);
        //vm.CategoryItems()[index].IsActive(!data.IsActive());
        if(confirm('Are you sure to delete?'))
        {
            $.ajax({
                    type: 'POST',
                    crossDomain :true,
                    contentType: 'application/json; charset=utf-8',
                    url:'http://localhost:8000/DeleteCategory',
                    dataType: 'json',
                    data: JSON.stringify({Id:data.Id()}),
                    async:false
                }).done(function (result) {
                    $('#example').DataTable().row( $(el).parents('tr')).remove().draw();
                }).fail(function (error) {
                });
            console.log(data);
        }
        //alert( data[0] +"'s salary is: "+ data[ 1 ] );
    });
    }
    };
ko.applyBindings(vm);
</script>
@stop

