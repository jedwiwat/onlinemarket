@extends('layouts.app')
@section('style')
h1 {
    display: inline-block;
    background-color: #333;
    color: #fff;
    font-size: 20px;
    font-weight: normal;
    text-transform: uppercase;
    padding: 20px;
}
@stop
@section('content')



@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="wrapper row">
    <div class="col-md-10 col-md-offset-1">

    <h1>Add Product</h1>
        {!! Form::open(array('url' => '/add', 'method' => 'post','style'=>'border:1px solid;border-radius:5px;border-color:gray;padding:10px;','files'=>true,'accept-charset'=>'UTF-8')) !!} 
        
        <div class="form-group">
        <input type="hidden" name="Id" data-bind="value:Id">
        {!! Form::label('Category', 'Category:') !!}
        <select id="Category" class="form-control" data-bind="options: CategoryItems,
               optionsText: 'Name',
               optionsValue: 'Id',
               value: SelectedCategory,
               optionsCaption: ''" required></select>
        <input type="hidden" name="CategoryId" data-bind="value:SelectedCategory">
        </div>
           
        <div class="form-group">
        {!! Form::label('Code', 'Code:') !!}
        {!! Form::text('Code', null, ['class'=>'form-control','required','data-bind'=>'value:Code']) !!}
        </div>

        <div class="form-group">
        {!! Form::label('Name', 'Name:') !!}
        {!! Form::text('Name', null, ['class'=>'form-control','required','maxlength'=>'26','data-bind'=>'value:Name']) !!}
        </div>
        <div class="form-group">
        {!! Form::label('Price', 'Price:') !!}
        {!! Form::number('Price', null, ['class'=>'form-control','required','data-bind'=>'value:Price']) !!}
        </div>
          <div class="form-group">
        {!! Form::label('PromotionPrice', 'Promotion Price:') !!}
        {!! Form::number('PromotionPrice', null, ['class'=>'form-control','data-bind'=>'value:PromotionPrice']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('image', 'Choose an image') !!}
            {!! Form::file('image',['id' => 'imgInp']) !!}
        </div>

        <div class="form-group">
            <input type="checkbox" id="toggle-trigger" name="IsActive" checked data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger" data-bind="checked:IsActive">
        </div>

        {{Form::submit('Save',array('class' => 'btn btn-default'))}}
        {{Form::reset('Cancel',array('class' => 'btn btn-default'))}}
        {!! Form::close() !!}
    </div>
</div>
<div class="wrapper row">
    <div class="col-md-10 col-md-offset-1">
    <table id="example" class="display cell-border" cellspacing="0" width="100%" data-bind="dataTable:triggerDataTable">
        <thead>
            <tr>
                <th>Picture</th>
                <th>Code</th>
                <th>Name</th>
                <th>Status</th>
                <th>Price</th>
                 <th>Promotion</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    </div>
</div>
<br><br>

<script>
var Product = function (data) {
    var self = this;
    self.CategoryId = ko.observable(data.CategoryId),
    self.Id = ko.observable(data.Id),
    self.Code = ko.observable(data.Code),
    self.Name = ko.observable(data.Name),
    self.IsActive = ko.observable(data.IsActive)
    self.Pic = ko.observable(data.Image);
    self.Price = ko.observable(data.Price);
    self.PromotionPrice = ko.observable(data.PromotionPrice);
};
var vm = new function() {
        var self = this;

        self.triggerDataTable = ko.observable(true);
        self.Id = ko.observable();
        self.SelectedCategory = ko.observable();
        self.Name = ko.observable();
        self.Code = ko.observable();
        self.IsActive = ko.observable();
        self.Price = ko.observable();
        self.PromotionPrice = ko.observable();
        self.IsActive = ko.observable(1);

        self.CategoryItems = ko.observableArray();
        self.ProductItems = ko.observableArray();
        self.IsActive.subscribe(function(newValue)
            {
                if(newValue ==0)
                    $('#toggle-trigger').prop('checked', false).change()
                else
                    $('#toggle-trigger').prop('checked', true).change()
            });
        self.GetCategory = function()
        {
            $.ajax({
                type: 'GET',
                crossDomain :true,
                contentType: 'application/json; charset=utf-8',
                url:'http://localhost:8000/GetCategory',
                dataType: 'json',
                async:false
            }).done(function (result) {
                self.CategoryItems(result);
            }).fail(function (error) {
               alert(error);
            });
        }
        self.GetProduct = function()
        {
            var self= this;
            $.ajax({
                type: 'GET',
                crossDomain :true,
                contentType: 'application/json; charset=utf-8',
                url:'http://localhost:8000/GetProduct',
                dataType: 'json',
                async:false
            }).done(function (result) {
                var list = ko.observableArray();
                for(var i =0;i<result.length;i++)
                {
                    var a = new Product(result[i]);
                    list.push(a);
                }
                self.ProductItems(list());
            }).fail(function (error) {
               alert(error);
            });
        }

        self.GetCategory();
    };
    ko.bindingHandlers.dataTable = {

    init: function(element, valueAccessor) {
        vm.GetProduct();
        $('#example').DataTable({
            data : vm.ProductItems(),
            responsive: true,
            scrollX:true,
            // ajax: 
            // {
            //    url: 'http://localhost:8000/GetCategory',
            //    type: 'get',
            //    dataSrc: function ( json ) {
            //      debugger;
            //       return json;
            //     }
            // },
            columns: [
            // { data: 'Id' },
                {
                    sortable: false,
                    render: function ( data, type, full, meta ) {
                     var Image = full.Pic;
                     var src = Image();

                    return "<img src=\""+src+"\" width=100 height=100>";
                     // return "<input type=\"checkbox\" data-bind=\"checked: "+IsActive()+" ==1 ? 'on':null\" name=\"IsActive\" checked data-toggle=\"toggle\" data-on=\"Active\" data-off=\"Inactive\" data-onstyle=\"success\" data-offstyle=\"danger\"/><button class='btn btn-danger'>Delete</button>";
                    }
                },
                { data: 'Code()' },
                { data: 'Name()' },
                { data: 'IsActive()' },
                { data: 'Price()' },
                { data: 'PromotionPrice()' },

                {
                    sortable: false,
                    render: function ( data, type, full, meta ) {
                     var IsActive = full.IsActive;
                     var btn = "";
                    if(IsActive())
                    {
                        btn = "<input type=\"button\" style=\"width: 75px;\" class=\"btn btn-success\" data-toggle=\"button\" aria-pressed=\"false\" autocomplete=\"off\" value=\"Active\">";
                    }
                    else
                    {
                         btn = "<input type=\"button\" style=\"width: 75px;\" class=\"btn btn-danger\" data-toggle=\"button\" aria-pressed=\"false\" autocomplete=\"off\" value=\"Inactive\">";
                    }
                    return btn+" <button id=\"delete\" class='btn btn-danger' style=\"width: 75px;\">Delete</button> <button id=\"edit\" class='btn btn-danger' style=\"width: 75px;\">Edit</button>";
                     // return "<input type=\"checkbox\" data-bind=\"checked: "+IsActive()+" ==1 ? 'on':null\" name=\"IsActive\" checked data-toggle=\"toggle\" data-on=\"Active\" data-off=\"Inactive\" data-onstyle=\"success\" data-offstyle=\"danger\"/><button class='btn btn-danger'>Delete</button>";
                 }
                }
            ],
        });
    },
    update: function(element, valueAccessor){
        //ko.unwrap(valueAccessor());
        $('#example tbody').on( 'click', ' input[type="button"]', function () {
        var data = $('#example').DataTable().row($(this).parents('tr')).data();
        //var index = $('#example').DataTable().column(0).data().indexOf( data.Name() );
        //index = (vm.CategoryItems().length- index)-1;
        //var cell = $('#example').DataTable().cell(index,1);
        var el = this;
        var index = vm.ProductItems().indexOf(data);
        
        $.ajax({
                type: 'POST',
                crossDomain :true,
                contentType: 'application/json; charset=utf-8',
                url:'http://localhost:8000/ToggleProduct',
                dataType: 'json',
                data: JSON.stringify({Id:data.Id()}),
                async:false
            }).done(function (result) {
                vm.ProductItems()[index].IsActive(data.IsActive() == true? 0:1);
                $('#example').DataTable().row(index).invalidate().draw();
                // var a = $("#example").dataTable()
                // a.fnDestroy();
               
                //$('#example').DataTable().row($(el).parents('tr')).draw();
            }).fail(function (error) {
            });


        console.log(data);
        //alert( data[0] +"'s salary is: "+ data[ 1 ] );
    });

    $('#example tbody').on( 'click', '#delete', function () {
        var data = $('#example').DataTable().row($(this).parents('tr')).data();
        var el = this;
        var index = vm.ProductItems().indexOf(data);
        //vm.CategoryItems()[index].IsActive(!data.IsActive());
        if(confirm('Are you sure to delete?'))
        {
            $.ajax({
                    type: 'POST',
                    crossDomain :true,
                    contentType: 'application/json; charset=utf-8',
                    url:'http://localhost:8000/DeleteProduct',
                    dataType: 'json',
                    data: JSON.stringify({Id:data.Id()}),
                    async:false
                }).done(function (result) {
                    $('#example').DataTable().row( $(el).parents('tr')).remove().draw();
                }).fail(function (error) {
                });


            console.log(data);
        }
        //alert( data[0] +"'s salary is: "+ data[ 1 ] );
    });

    $('#example tbody').on( 'click', '#edit', function () {
        var data = $('#example').DataTable().row($(this).parents('tr')).data();
        var el = this;
        var index = vm.ProductItems().indexOf(data);
        vm.Id(vm.ProductItems()[index].Id());
        vm.SelectedCategory(vm.ProductItems()[index].CategoryId());
        vm.Code(vm.ProductItems()[index].Code());
        vm.Name(vm.ProductItems()[index].Name());
        vm.Price(vm.ProductItems()[index].Price());
        vm.PromotionPrice(vm.ProductItems()[index].PromotionPrice());
        vm.IsActive(vm.ProductItems()[index].IsActive());
        //vm.CategoryItems()[index].IsActive(!data.IsActive());
        console.log(data);
        //alert( data[0] +"'s salary is: "+ data[ 1 ] );
    });
    }
};
    ko.applyBindings(vm);
</script>
@stop

