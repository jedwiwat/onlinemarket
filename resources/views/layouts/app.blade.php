<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="{!! asset('css/font-awesome.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/bootstrap.toggle.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/bootstrap.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/dataTable.css') !!}" media="all" rel="stylesheet" type="text/css" />
    {{-- <link href="{!! asset('css/cerulean.min.css') !!}" media="all" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="{!! asset('css/lumen.min.css') !!}" media="all" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="{!! asset('css/paper.min.css') !!}" media="all" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="{!! asset('css/sandstone.min.css') !!}" media="all" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="{!! asset('css/slate.min.css') !!}" media="all" rel="stylesheet" type="text/css" /> --}}
    {{-- <link href="{!! asset('css/united.min.css') !!}" media="all" rel="stylesheet" type="text/css" /> --}}
     {{-- <link href="http://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.js" media="all" rel="stylesheet" type="text/css" /> --}}
   
    <!-- JavaScripts 
    <script type="text/javascript" src="http://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.js"></script>
    -->
    <script type="text/javascript" src="{!! asset('js/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/bootstrap.toggle.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/knockout-3.4.0.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/dataTable.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/jquery-ui.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/pagination.js') !!}"></script>
    
   
    <!--<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- Styles -->
    <style>
        body {
            font-family: 'Lato';
            margin-top: 0px;
            
        }
        .companyLogo
        {
           font: normal 36px 'Cookie', cursive;

        }
        .footer
        {
            padding: 55px;
            position: static;
            bottom: 0;
            color: white;
        }
        .bg-light-gray
        {
            background-color: #292c2f;
            box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.12);
        }
        .fa-btn {
            margin-right: 6px;
        }
       .footer div
        {
            display: block;
            text-align: left;
            color: #ffffff;
        }
        .footer i
        {
            background-color: #33383b;
            color: #ffffff;
            font-size: 25px;
            width: 38px;
            height: 38px;
            border-radius: 50%;
            text-align: center;
            line-height: 42px;
            margin: 10px 15px;
            vertical-align: middle;
        }
        .footer p
        {
            display: inline-block;
            vertical-align: middle;
            margin: 0;
        }
        .footer p span
        {
            display: block;
            vertical-align: middle;
            margin: 0;
        }
        .footer div span
        {
            font-weight: bold;
            color:red;
        }
        .footer-icons i
        {
            background-color: #33383b;
            color: #ffffff;
            font-size: 25px;
            width: 38px;
            height: 38px;
            border-radius: 10%;
            text-align: center;
            line-height: 42px;
            margin: 0px 10px 0px 0px;
            vertical-align: middle;
        }
        .about 
        {
            vertical-align: middle;
        }
        .wrapper 
        {
            margin-top: 10px;
        }
    @yield('style')

    </style>

</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Moo Stock
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    {{-- <li><a href="{{ url('/home') }}">Home</a></li> --}}
                    <li><a href="{{ url('/show') }}">Show</a></li>
                    @if (Auth::user()['role'] == 'admin')
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           <li><a href="{{ url('/manageProduct') }}">Product</a></li>
                            <li><a href="{{ url('/manageCategory') }}">Category</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
<div class="container-fluid">
@yield('banner')
@yield('content')
<div class="row bg-light-gray footer">
    <div class="col-sm-4">
        <div class="companyLogo">
           Company<span>Logo</span>
        </div>
    </div>

    <div class="col-sm-4">
        <div>
            <i class="fa fa-map-marker"></i>
            <p>21 Revolution Street<br>Paris, France</p>
        </div>
        <div>
            <i class="fa fa-phone"></i>
            <p>+1 555 123456</p>
        </div>
        <div>
            <i class="fa fa-envelope"></i>
            <p>support@company.com</p>
        </div>
    </div>    
    <div class="col-sm-4">
        <div class="about">
            <div>
                <p>
                <span>About the company</span>
                Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
            </p>
            </div>
            <div class="footer-icons">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
                <a href="#"><i class="fa fa-github"></i></a>
            </div>
        </div>
    </div>
    
</div>



{{-- 
    <div class="row bg-light-gray footer">
        <div class="footer-center">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="companyLogo">
                   Company<span>Logo</span>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div>
                    <i class="fa fa-map-marker"></i>
                    <p>21 Revolution Street<br>Paris, France</p>
                </div>
                <div>
                    <i class="fa fa-phone"></i>
                    <p>+1 555 123456</p>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div>
                    <p>
                    <span>About the company</span>
                    Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
                </p>
                </div>
            </div>

               
        </div>
    </div>
</div> --}}

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
