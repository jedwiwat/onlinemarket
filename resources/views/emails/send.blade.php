<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
  		body
  		{
  			 font-family: "Times New Roman", Times, serif;
  			 margin: 0px;
  		}
  		.content > tbody > tr,.content > thead 
  		{
  				border-top: 1px solid;	
  				border-bottom: 1px solid;
  		}
  		.content > tbody > tr:last-child
  		{
  			border:0px;
  		}
  		.header > tbody > tr > td:nth-child(odd)
  		{
  			text-align: right;
  		}
  </style>
  </head>
<body>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
	<tr height="200" align="center">
		<td bgcolor="#70bbd9" style="font-size:20px;">
		<table class="header" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
		<tr>
			<td>Name :</td>
			<td>{{ Auth::user()->name }}</td>
			<td>Tel : </td>
			<td>{{ Auth::user()->name }}</td>
		</tr>
		<tr>
			<td>Address :</td>
			<td>{{ Auth::user()->name }}</td>
			<td></td>
			<td></td>
		</tr>
		</table>
		</td>
	</tr>
	<tr> 
	<!-- Content -->
		<td style="padding:20px;">
		<table class="content" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
			<thead>
				<tr>
				<th width="50" align="center">No.</th>
				<th width="150" align="center">Code</th>
				<th width="150" align="center">Name</th>
				<th width="150" align="center">Piece(s)</th>
				<th width="150" align="center">Price</th>
				</tr>
			</thead>
<!-- Foreach -->
			<tbody>
			@foreach ($items as $key=>$item)
			<?php 
			    $total = 0;
			    foreach($items as $score)
			        $total += $score['product']['Price'];
			?>
			<tr>
				<td align="center">{{ $key+1 }}</td>
				<td align="center">{{ $item['product']['Code']}}</td>
				<td align="center">{{ $item['product']['Name']}}</td>
				<td align="center">{{ $item['Count']}} piece(s)</td>
				<td align="right">{{ $item['product']['Price']}} Baht</td>
			</tr>
			@endforeach
			<tr>
				<td height="30"></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align="right">Total : {{ $total }} Baht</td>
			</tr>
			</tbody>
			</table>
		</td>
	</tr>
	<tr height="200" align="center">
		<td  bgcolor="#ee4c50" style="font-size:70px;">Footer</td>
	</tr>
</table>

</body>
</html>

