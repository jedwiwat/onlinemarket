<?php

namespace App\Http\Middleware;

use Closure;

class RouteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $request->Code ==> post method
        
        // if($request->Code=="111")
        // {
        //       return redirect('test');
        // }
        return $next($request);
    }
}
