<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Request;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");




Route::get('/showPicture','ProductController@showPicture');
Route::get('/GetProduct','ProductController@Get');
Route::get('/GetProductWithPagination','ProductController@GetProductWithPagination');
Route::post('/DeleteProduct','ProductController@Delete');
Route::post('/ToggleProduct','ProductController@Toggle');

Route::post('/ToggleCategory','CategoryController@Toggle');
Route::post('/DeleteCategory','CategoryController@Delete');
Route::get('/GetCategory','CategoryController@Get');
Route::post('/AddCategory','CategoryController@Add');

Route::get('/show','ProductController@ShowPage');

Route::group(['middleware' => 'auth'], function () {
    Route::post('/add','ProductController@Add');
    Route::post('/update','ProductController@Update');
    Route::post('/delete','ProductController@Delete');
    Route::post('/payment','PaymentController@Payment');
    
});
Route::auth();
Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');

Route::get('/sendmail', function()
    {
       return view('emails.send',['items'=>'','title' => '','content' => '']);
    });
Route::get('/manageProduct', function()
    {
       return view('manageProduct');
    });
Route::get('/manageCategory', function()
    {
       return view('manageCategory');
    });

// Route::any('{catchall}', function() {
//   //some code
//     return redirect('/');
// })->where('catchall', '.*');

