<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use Webpatser\Uuid\Uuid;
class CategoryController extends Controller
{
    //
    public function Add(Request $request)
    {
        $this->validate($request, [
        'Name' => 'required'
        ]);
        $category = new Category;
        $category->Id = Uuid::generate();
        $category->Name = $request->Name;
        $category->IsActive = $request->IsActive == 'on'? 1:0;
        $category->save();
       return redirect('/manageCategory');
    }
    public function Toggle(Request $request)
    {
        //dd($request->Id);
    	$result = Category::find($request->Id);
    	$result->IsActive =  !$result->IsActive;
        $result->save();
    	return response()->json(true);
    }
    public function Get()
    {

    	$q = Category::orderBy('Name', 'ASC')->get();
    	return response()->json($q);
    }
    public function Delete(Request $request)
    {
        $q = Category::find($request->Id);
        $q->delete();
        return response()->json(true);
    }
}
