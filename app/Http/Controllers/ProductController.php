<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use DB;
use App\Http\Requests;
use App\Product;
use Webpatser\Uuid\Uuid;
use Mail;
use App\User;
use Image;
class ProductController extends Controller
{
    //
    public function ShowPage()
    {
        return view('show');
    }
    public function Index()
    {
        return view('manageProduct');
    }
    public function Add(Request $request)
    {
         $this->validate($request, [
        'CategoryId' => 'required',
        'Code' => 'required',
        'Name' => 'required|max:26',
        'Price' => 'required',
        ]);
        if($request->Id !="")
        {
            $this->Update($request);
        }
        else
        {
            $product = new Product;
            $product->Id = Uuid::generate();
            $product->CategoryId = $request->CategoryId;
            if($request->file('image') != null)
            {
            $file = $request->file('image');
            //$mimeType = $file->getMimeType();
            $img = Image::make($file)->resizeCanvas(800, 500);
            Response::make($img->encode('jpeg'));
            //dd($mimeType);
            //var_dump($request->IsActive);
            $product->Image = $img;
            }
            $product->Name = $request->Name;
            $product->Code = $request->Code;
            $product->Price = $request->Price;
            $product->PromotionPrice = $request->PromotionPrice;
            $product->IsActive = $request->IsActive == 'on'? 1:0;
            $product->save();
        }
       return redirect('/manageProduct');

        //return response()->json($product);
        
    }
    public function Update($request)
    {
        $result = Product::find($request->Id);
        $result->CategoryId = $request->CategoryId;
        $result->Code = $request->Code;
        $result->Name = $request->Name;
        if($request->file('image') != null)
        {
            $file = $request->file('image');
            //$mimeType = $file->getMimeType();
            $img = Image::make($file)->resizeCanvas(800, 500);
            Response::make($img->encode('jpeg'));
            //dd($mimeType);
            //var_dump($request->IsActive);
            $result->Image = $img;
        }
        $result->Price = $request->Price;
        $result->PromotionPrice = $request->PromotionPrice;
        $result->IsActive = $request->IsActive == 'on'? 1:0;
        $result->save();
    }

    public function GetProductWithPagination(Request $request)
    {
        // //$products = Product::orderBy('Code', 'ASC')->Where('IsActive',1)->get();
        $query = DB::table('products')
            ->select(['*',
                DB::raw('CONCAT("data:image/jpeg;base64,", TO_BASE64(products.Image)) as Image')
                ])->where('IsActive',1)->paginate($request->limit);
            
        return response()->json($query);
   }
   public function Get()
    {
        // //$products = Product::orderBy('Code', 'ASC')->Where('IsActive',1)->get();
        $query = DB::table('products')
            ->select(['*',
                DB::raw('CONCAT("data:image/jpeg;base64,", TO_BASE64(products.Image)) as Image')
                ])->get();
            
        return response()->json($query);
   }
     public function showPicture()
    {
        $picture = Product::findOrFail('aa038c60-6bb9-11e6-8a78-2ba01680848e');
        $pic = Image::make($picture->Image);
        $response = Response::make($pic->encode('jpeg'));

        //setting content-type
        $response->header('Content-Type', 'image/jpeg');

        return $response;
    }

    public function Delete(Request $request)
    {
        $q = Product::find($request->Id);
        $q->delete();
        return response()->json(true);
    }

    public function Toggle(Request $request)
    {
        //dd($request->Id);
        $result = Product::find($request->Id);
        $result->IsActive =  !$result->IsActive;
        $result->save();
        return response()->json(true);
    }
}