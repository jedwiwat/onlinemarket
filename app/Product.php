<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
     protected $table = 'products';
     Protected $primaryKey = "Id";
     public $incrementing = false;
}
