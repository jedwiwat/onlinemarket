<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('Id');
            $table->primary('Id');
            $table->string('CategoryId');
            $table->string('Code');
            $table->string('Name');
            $table->integer('Price');
            $table->integer('PromotionPrice')->nullable();
            $table->boolean('IsActive');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE products ADD  Image LONGBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
